package com.twuc.webApp.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Table(name = "productlines")
@Entity
public class ProductLineEntity {
    @Id
    @Column(nullable = false, length = 50)
    private String productLine;

    @Column
    @Size(max = 4000)
    private String textDescription;

    public ProductLineEntity() {
    }

    public ProductLineEntity(String productLine, String textDescription) {
        this.productLine = productLine;
        this.textDescription = textDescription;
    }

    public String getProductLine() {
        return productLine;
    }

    public String getTextDescription() {
        return textDescription;
    }

    public void setTextDescription(String textDescription) {
        this.textDescription = textDescription;
    }
}
