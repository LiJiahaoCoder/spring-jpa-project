package com.twuc.webApp.controllers;

import com.twuc.webApp.entities.ProductEntity;
import com.twuc.webApp.entities.ProductLineEntity;
import com.twuc.webApp.repositories.ProductLineRepository;
import com.twuc.webApp.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityManager;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductController {
    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductLineRepository productLineRepository;

    @Autowired
    EntityManager entityManager;

    @PostMapping("/products")
    public ResponseEntity createProduct(@RequestBody ProductEntity productEntity) {
        productRepository.saveAndFlush(productEntity);
        entityManager.clear();

        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PostMapping("/productlines")
    public ResponseEntity createProductLine(@RequestBody ProductLineEntity productLineEntity) {
        productLineRepository.saveAndFlush(productLineEntity);
        entityManager.clear();

        return ResponseEntity.status(HttpStatus.CREATED).build();

    }

    @GetMapping("/products")
    public ResponseEntity<List<ProductEntity>> getProducts() {
        List<ProductEntity> products = productRepository.findAll();

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(products);
    }

    @GetMapping("/productlines")
    public ResponseEntity<List<ProductLineEntity>> getProductLines() {
        List<ProductLineEntity> productLines = productLineRepository.findAll();

        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(productLines);
    }
}
