package com.twuc.webApp.repositories;

import com.twuc.webApp.entities.ProductLineEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductLineRepository extends JpaRepository<ProductLineEntity, String> {
}
