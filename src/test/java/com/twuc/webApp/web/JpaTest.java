package com.twuc.webApp.web;

import com.twuc.webApp.entities.ProductEntity;
import com.twuc.webApp.entities.ProductLineEntity;
import com.twuc.webApp.repositories.ProductLineRepository;
import com.twuc.webApp.repositories.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class JpaTest extends JpaTestBase{
    @Autowired
    ProductRepository productRepository;

    @Autowired
    ProductLineRepository productLineRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    void should_create_and_get_products_and_productlines() {
        ProductLineEntity productLineEntity = new ProductLineEntity(
                "line_1",
                "This is first product line."
        );
        ProductEntity productEntity = new ProductEntity(
                "product_code_1",
                "product_name_1",
                null,
                "scale_1",
                "product_vendor_1",
                "product_description_1",
                (short) 1,
                BigDecimal.valueOf(55.5),
                BigDecimal.valueOf(88.8)
        );

        flushAndClear(em -> {
            productEntity.setProductLine(productLineEntity);
            productRepository.save(productEntity);
        });

        int productsAmount = productRepository.findAll().size();
        int productLinesAmount = productLineRepository.findAll().size();

        assertEquals(1, productsAmount);
        assertEquals(1, productLinesAmount);
    }
}