package com.twuc.webApp.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.function.Consumer;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class JpaTestBase {
    @Autowired
    private EntityManager em;

    protected void flushAndClear(Consumer<EntityManager> consumer) {
        try {
            consumer.accept(em);
        } finally {
            em.flush();
            em.clear();
        }
    }
}
