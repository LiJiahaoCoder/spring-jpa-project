package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.entities.ProductEntity;
import com.twuc.webApp.entities.ProductLineEntity;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ControllerTest extends ApiTestBase {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Test
    void should_create_product() throws Exception {
        ProductEntity productEntity = new ProductEntity(
                "product_code_1",
                "product_name_1",
                null,
                "scale_1",
                "product_vendor_1",
                "product_description_1",
                (short) 1,
                BigDecimal.valueOf(55.5),
                BigDecimal.valueOf(88.8)
        );

        getMockMvc().perform(post("/api/products")
                .content(objectMapper.writeValueAsString(productEntity))
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().is(201));
    }

    @Test
    void should_create_a_product_line() throws Exception {
        ProductLineEntity productLineEntity = new ProductLineEntity(
                "line_1",
                "This is first product line."
        );

        getMockMvc().perform(post("/api/productlines")
                .content(objectMapper.writeValueAsString(productLineEntity))
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().is(201));
    }

    @Test
    void should_get_all_products() throws Exception {
        ProductEntity productEntity = new ProductEntity(
                "product_code_1",
                "product_name_1",
                null,
                "scale_1",
                "product_vendor_1",
                "product_description_1",
                (short) 1,
                BigDecimal.valueOf(55.5),
                BigDecimal.valueOf(88.8)
        );
        ProductEntity anotherProductEntity = new ProductEntity(
                "product_code_2",
                "product_name_2",
                null,
                "scale_2",
                "product_vendor_2",
                "product_description_2",
                (short) 2,
                BigDecimal.valueOf(55.5),
                BigDecimal.valueOf(88.8)
        );

        getMockMvc().perform(post("/api/products")
                .content(objectMapper.writeValueAsString(productEntity))
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().is(201));
        getMockMvc().perform(post("/api/products")
                .content(objectMapper.writeValueAsString(anotherProductEntity))
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().is(201));

        MvcResult result = getMockMvc().perform(get("/api/products"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        List list = objectMapper.readValue(contentAsString, List.class);

        assertEquals(2, list.size());
    }

    @Test
    void should_get_all_product_lines() throws Exception {
        ProductLineEntity productLine = new ProductLineEntity("line_1", "This is my first product line.");

        getMockMvc().perform(post("/api/productlines")
                .content(objectMapper.writeValueAsString(productLine))
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().is(201));

        MvcResult result = getMockMvc().perform(get("/api/productlines"))
                .andExpect(status().is(200))
                .andReturn();

        String contentAsString = result.getResponse().getContentAsString();

        List list = objectMapper.readValue(contentAsString, List.class);

        assertEquals(1, list.size());
    }
}
